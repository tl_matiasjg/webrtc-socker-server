var express = require('express');
var socket = require('socket.io');

// App setup
var app = express();
var server = app.listen(4000, function(){
    console.log('listening for requests on port 4000,');
});

// Static files
app.use(express.static('public'));

let users = [];

// Socket setup & pass server
var io = socket(server);
io.on('connection', (socket) => {
    socket.emit('login', {users, id: socket.id});
    
    socket.on('connectuser', function(data){
        const obj = {
            name: data.name,
            id: socket.id
        };
        if (users.filter(u => u.name === data.name).length === 0) {
            users.push(obj);
        }
        io.sockets.emit('userconnected', obj);
    });

    socket.on('logout', function(data){
        console.log("logout", { id: socket.id });
        users = users.filter(u => u.id !== socket.id);
        io.sockets.emit('logout', { id: socket.id });
    });

    socket.on('signaling', function(data){
        io.sockets.emit('signaling', data);
    });

    // Handle chat event
    socket.on('message', function(data){
        console.log('message', data);
        io.sockets.emit('message', data);
    });

    socket.on('disconnect', (reason) => {
        console.log("disconnect");
        users = users.filter(u => u.id !== socket.id);
        io.sockets.emit('logout', { id: socket.id });
    });
});